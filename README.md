# README #

Spring boot + maven backend application.

### What is this repository for? ###

This is a demo version of meter reading application.

* Quick summary
* Version 0.0.1

### How do I get set up? ###

To install this example application, run the following commands:

```bash
git clone https://davobo@bitbucket.org/davobo/typeqast.git
cd typeqast
```

This will get a copy of the project installed locally. To compile all of its dependencies and start application, follow the instructions below.

To compile application, run:
```bash
mvn package
```

To start the server, run:
```bash
cd target
java -jar demo-0.0.1-SNAPSHOT.jar
```

Meter data by year, base URL:
```bash
http://localhost:8080/api/client/1/year
```

Meter data by the specific year, base URL:
```bash
http://localhost:8080/api/client/1/year/2018
```

Meter data by the specific year, base URL:
```bash
http://localhost:8080/api/client/1/year/2018/1
```

### Extra info ###

For easier testing of application endpoints, a Postman configuration file "Typeqast.postman_collection.json" is added to
root of repository. Importing it to Postman application, provides access to all REST endpoints of demo application. 

After running building package (or running test), code coverage can be found at:
target\site\jacoco

### Future work ###

* Implement logging through Slf4j
* Add OpenApi
* Improve test coverage for different input values and edge cases.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact