package com.typeqast.demo.helper;

import com.typeqast.demo.controller.MeterDataController;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonth;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonthDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificYearDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataByYearsDTO;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.database.model.Meter;
import com.typeqast.demo.database.model.MeterData;
import com.typeqast.demo.service.meter.MeterEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class CustomSuplier {

    public static Client createClientWithId(Long id) {
        return Client.builder()
                .clientId(id)
                .address("some address" + id)
                .email("test.user" + id + "@gmail.com")
                .firstName("test")
                .lastName("user" + id)
                .meter(Meter.builder().meterId(1L).name(MeterEnum.ELECTRIC).build())
                .build();
    }

    public static Page<MeterDataByYearsDTO> createMeterDataByYear() {
        MeterDataByYearsDTO meterDataByYearsDTO = new MeterDataByYearsDTO(2018, 35);
        PageRequest pageRequest = PageRequest.of(0, 20);
        return new PageImpl<MeterDataByYearsDTO>(List.of(meterDataByYearsDTO), pageRequest, 1);
    }

    public static MeterDataBySpecificYearDTO createMeterDataBySpecificYear() {
        List<MeterDataBySpecificMonth> meterDataBySpecificMonth = List.of(
                new MeterDataBySpecificMonth(1, 2L),
                new MeterDataBySpecificMonth(2, 5L),
                new MeterDataBySpecificMonth(3, 7L),
                new MeterDataBySpecificMonth(4, 21L));
        return new MeterDataBySpecificYearDTO(2018, meterDataBySpecificMonth);
    }

    public static MeterDataBySpecificMonthDTO createMeterDataBySpecificMonth() {
        return new MeterDataBySpecificMonthDTO(2018, new MeterDataBySpecificMonth(2, 5L));
    }

    public static MeterData createMeterDataWithId(Long id) {
        return MeterData.builder()
                .meterDataId(id)
                .client(createClientWithId(1L))
                .date(LocalDate.of(2018, 1, 5))
                .quantity(2L)
                .build();
    }

    public static MeterData createMeterDataWithId(Long id, LocalDate date, Long quantity) {
        MeterData meterData = createMeterDataWithId(id);
        meterData.setDate(date);
        meterData.setQuantity(quantity);
        return meterData;
    }

    public static Meter createMeterWithId(Long id, MeterEnum meterEnum) {
        return Meter.builder().meterId(id).name(meterEnum).build();
    }
}
