package com.typeqast.demo.integrationTest.service;

import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.service.client.ClientService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class ClientServiceIT {

    @Resource
    private ClientService clientService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created client ID
    private static long CREATED_CLIENT_ID = 3L;

    @Test
    public void getMultipleUsers() {

        Page<Client> allClients = clientService.findAll(PageRequest.of(0, 10));
        List<Client> listOfClients = allClients.getContent();
        assertEquals(2, allClients.getTotalElements(), "Database doesn't contain expected number of clients.");
        assertEquals(1, listOfClients.get(0).getClientId().longValue(), "Database doesn't contain client with id: " + listOfClients.get(0).getClientId());
        assertEquals(2, listOfClients.get(1).getClientId().longValue(), "Database doesn't contain client with id: " + listOfClients.get(1).getClientId());
        assertEquals(allClients.getTotalPages(), 1);
    }

    @Test
    public void getUser() {
        Client client = clientService.findById(1L);

        assertEquals(1L, client.getClientId().longValue(), "User with wrong Id:" + client.getClientId() + " returned from database");
        assertEquals("test", client.getFirstName(), "User with unexpected first name.");
        assertEquals("user1", client.getLastName(), "User with unexpected last name.");
        assertEquals("some address1", client.getAddress(), "User with unexpected address.");
        assertEquals("test.user1@gmail.com", client.getEmail(), "User with unexpected email.");
    }

    @Test
    @Order(1)
    public void addNewUser() {
        Client newClient = clientService.create(CustomSuplier.createClientWithId(CREATED_CLIENT_ID));
        CREATED_CLIENT_ID = newClient.getClientId();
        assertNotNull(clientService.findById(CREATED_CLIENT_ID));
    }

    @Test
    @Order(2)
    public void updateExistingUser() {
        Client newClient = Client.builder().address("updated address").build();
        clientService.update(CREATED_CLIENT_ID, newClient);
        Client updatedClient = clientService.findById(CREATED_CLIENT_ID);

        assertEquals("updated address", updatedClient.getAddress());
    }

    @Test
    @Order(3)
    public void deleteUser() {
        clientService.delete(CREATED_CLIENT_ID);
        assertThrows(ResourceNotFoundException.class, () -> clientService.findById(CREATED_CLIENT_ID));
    }
}
