package com.typeqast.demo.integrationTest.service;

import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonth;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonthDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificYearDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataByYearsDTO;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.database.model.MeterData;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.service.meterData.MeterDataService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class MeterDataServiceIT {

    @Resource
    private MeterDataService meterDataService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created meter ID
    private static long CREATED_METER_DATA_ID = 3L;

    @Test
    public void getMultipleMeters() {

        Page<MeterData> allMeterData = meterDataService.findAll(PageRequest.of(0, 10));
        List<MeterData> listOfMeterData = allMeterData.getContent();
        assertEquals(4, allMeterData.getTotalElements(), "Database doesn't contain expected number of data.");
        assertEquals(1, listOfMeterData.get(0).getMeterDataId(), "Database doesn't contain meter data with id: " + listOfMeterData.get(0).getMeterDataId());
        assertEquals(2, listOfMeterData.get(1).getMeterDataId(), "Database doesn't contain meter data with id: " + listOfMeterData.get(1).getMeterDataId());
        assertEquals(3, listOfMeterData.get(2).getMeterDataId(), "Database doesn't contain meter data with id: " + listOfMeterData.get(2).getMeterDataId());
        assertEquals(4, listOfMeterData.get(3).getMeterDataId(), "Database doesn't contain meter data with id: " + listOfMeterData.get(3).getMeterDataId());
        assertEquals(2, listOfMeterData.get(0).getQuantity(), "Database doesn't contain meter data with correct quantity");
        assertEquals(5, listOfMeterData.get(1).getQuantity(), "Database doesn't contain meter data with correct quantity");
        assertEquals(7, listOfMeterData.get(2).getQuantity(), "Database doesn't contain meter data with correct quantity");
        assertEquals(21, listOfMeterData.get(3).getQuantity(), "Database doesn't contain meter data with correct quantity");
    }

    @Test
    public void getMeterData() {
        MeterData meterData = meterDataService.findById(1L);

        assertEquals(1, meterData.getMeterDataId(), "Meter data with wrong Id:" + meterData.getMeterDataId() + " returned from database");
        assertEquals(2, meterData.getQuantity(), "Database doesn't contain meter data with correct quantity");
    }

    @Test
    public void getMeterDataByYear() {
        Page<MeterDataByYearsDTO> meterDataByYear = meterDataService.getMeterDataByYear(Client.builder().clientId(1L).build(), PageRequest.of(0, 10));
        assertEquals(1, meterDataByYear.getContent().size());
        assertEquals(35, meterDataByYear.getContent().get(0).getTotal());
    }

    @Test
    public void getMeterDataBySpecificYear() {
        MeterDataBySpecificYearDTO meterDataByYear = meterDataService.getMeterDataBySpecificYear(Client.builder().clientId(1L).build(), 2018);
        assertEquals(2018, meterDataByYear.getYear());
        assertEquals(4, meterDataByYear.getMeterDataBySpecificMonthList().size());
    }

    @Test
    public void getMeterDataBySpecificMonth() {
        MeterDataBySpecificMonthDTO meterDataByMonth = meterDataService.getMeterDataBySpecificMonth(Client.builder().clientId(1L).build(), 2018, 1);
        assertEquals(2018, meterDataByMonth.getYear());
        assertEquals(2, meterDataByMonth.getMeterDataBySpecificMonth().getValue());
    }

    @Test
    @Order(1)
    public void addNewMeter() {
        MeterData newMeterData = meterDataService.create(MeterData.builder()
                .date(LocalDate.of(2018, 10, 2))
                .quantity(5L)
                .build());
        CREATED_METER_DATA_ID = newMeterData.getMeterDataId();
        assertNotNull(meterDataService.findById(CREATED_METER_DATA_ID));
    }

    @Test
    @Order(2)
    public void updateExistingMeter() {
        MeterData newMeterData = MeterData.builder().quantity(3L).build();
        meterDataService.update(CREATED_METER_DATA_ID, newMeterData);
        MeterData updatedMeterData = meterDataService.findById(CREATED_METER_DATA_ID);

        assertEquals(3L, updatedMeterData.getQuantity());
    }

    @Test
    @Order(3)
    public void deleteMeter() {
        meterDataService.delete(CREATED_METER_DATA_ID);
        assertThrows(ResourceNotFoundException.class, () -> meterDataService.findById(CREATED_METER_DATA_ID));
    }
}
