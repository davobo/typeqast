package com.typeqast.demo.integrationTest.service;

import com.typeqast.demo.database.model.Meter;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.service.meter.MeterEnum;
import com.typeqast.demo.service.meter.MeterService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class MeterServiceIT {

    @Resource
    private MeterService meterService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created meter ID
    private static long CREATED_METER_ID = 3L;

    @Test
    public void getMultipleMeters() {

        Page<Meter> allMeters = meterService.findAll(PageRequest.of(0, 10));
        List<Meter> listOfMeters = allMeters.getContent();
        assertEquals(2, allMeters.getTotalElements(), "Database doesn't contain expected number of meters.");
        assertEquals(1, listOfMeters.get(0).getMeterId().longValue(), "Database doesn't contain meter with id: " + listOfMeters.get(0).getMeterId());
        assertEquals(2, listOfMeters.get(1).getMeterId().longValue(), "Database doesn't contain meter with id: " + listOfMeters.get(1).getMeterId());
        assertEquals(allMeters.getTotalPages(), 1);
    }

    @Test
    public void getMeter() {
        Meter meter = meterService.findById(1L);

        assertEquals(1L, meter.getMeterId().longValue(), "Meter with wrong Id:" + meter.getMeterId() + " returned from database");
        assertEquals(MeterEnum.ELECTRIC, meter.getName(), "Meter with unexpected name.");
    }

    @Test
    @Order(1)
    public void addNewMeter() {
        Meter newMeter = meterService.create(Meter.builder().name(MeterEnum.GAS).build());
        CREATED_METER_ID = newMeter.getMeterId();
        assertNotNull(meterService.findById(CREATED_METER_ID));
    }

    @Test
    @Order(2)
    public void updateExistingMeter() {
        Meter newMeter = Meter.builder().name(MeterEnum.POWER).build();
        meterService.update(CREATED_METER_ID, newMeter);
        Meter updatedMeter = meterService.findById(CREATED_METER_ID);

        assertEquals(MeterEnum.POWER, updatedMeter.getName());
    }

    @Test
    @Order(3)
    public void deleteMeter() {
        meterService.delete(CREATED_METER_ID);
        assertThrows(ResourceNotFoundException.class, () -> meterService.findById(CREATED_METER_ID));
    }
}
