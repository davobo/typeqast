package com.typeqast.demo.integrationTest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomConfiguration;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.service.client.ClientService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import({CustomConfiguration.class})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ClientControllerIT {

    @Resource
    private ClientService clientService;

    @LocalServerPort
    int randomServerPort;

    @Resource
    private RestTemplateBuilder restTemplateBuilder;

    @Resource
    ResourceLoader resourceLoader;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created client ID
    private static long CREATED_CLIENT_ID = 3L;


    @Test
    public void getSingleClient() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseClientExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/client/1", String.class);

        // Then
        // Remove random port
        assertNotNull(response, "Response returned for client with ID 1 is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void getSingleClientThatDoesNotExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseClientDoesntExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response;
        try {
            response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                    + "/api/client/3", String.class);
        } catch (HttpClientErrorException httpClientErrorException) {
            response = httpClientErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        JSONObject removeTimestamp = responseToMatch.getJSONObject("_embedded").getJSONObject("error");
        removeTimestamp.remove("timestamp");
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void givenThatClientWithSameAddressAlreadyExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseClientWithProvidedAddressAlreadyExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());
        Client client = CustomSuplier.createClientWithId(3L);
        client.setAddress("some address1");

        // When
        String response;
        try {
            response = restTemplateBuilder.build().postForObject("http://localhost:" + randomServerPort
                    + "/api/client", client, String.class);
        } catch (HttpClientErrorException httpClientErrorException) {
            response = httpClientErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        JSONObject removeTimestamp = responseToMatch.getJSONObject("_embedded").getJSONObject("error");
        removeTimestamp.remove("timestamp");
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void getMultipleClients() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMultipleClientsExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/client", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void getDataByYear() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMeterDataByYearExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/client/1/year", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void getDataBySpecificYear() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMeterDataBySpecificYearExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/client/1/year/2018", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void getDataBySpecificMonth() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMeterDataByMonthExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/client/1/year/2018/2", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    @Order(1)
    void createClient() throws Exception {
        // When
        String response = restTemplateBuilder.build().postForObject("http://localhost:" + randomServerPort
                + "/api/client", CustomSuplier.createClientWithId(3L), String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response).getJSONObject("_embedded").getJSONObject("client");
        responseToMatch.remove("_links");
        CREATED_CLIENT_ID = responseToMatch.getLong("clientId");
        String clientFromDatabase = new ObjectMapper().writer().writeValueAsString(clientService.findById(CREATED_CLIENT_ID));
        JSONObject expectedResponse = new JSONObject(clientFromDatabase);
        expectedResponse.getJSONObject("meter").remove("links");
        expectedResponse.remove("links");
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    @Order(2)
    void updateClient() throws Exception {
        // Given
        Client client = Client.builder().address("new address").build();

        // When
        restTemplateBuilder.build().put("http://localhost:" + randomServerPort
                + "/api/client/" + CREATED_CLIENT_ID, client);

        // Then
        Client updatedClient = clientService.findById(CREATED_CLIENT_ID);
        assertEquals(updatedClient.getAddress(), "new address");
    }

    @Test
    @Order(3)
    void deleteClient() throws Exception {
        // When
        restTemplateBuilder.build().delete("http://localhost:" + randomServerPort
                + "/api/client/" + CREATED_CLIENT_ID);

        // Then
        assertThrows(ResourceNotFoundException.class, () -> clientService.findById(CREATED_CLIENT_ID));
    }
}
