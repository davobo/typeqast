package com.typeqast.demo.integrationTest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomConfiguration;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.service.client.ClientService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import({CustomConfiguration.class})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MeterControllerIT {

    @Resource
    private ClientService clientService;

    @LocalServerPort
    int randomServerPort;

    @Resource
    private RestTemplateBuilder restTemplateBuilder;

    @Resource
    ResourceLoader resourceLoader;

    @Test
    void getAllMeters() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meter/ResponseMultipleMetersExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/meter", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }
}
