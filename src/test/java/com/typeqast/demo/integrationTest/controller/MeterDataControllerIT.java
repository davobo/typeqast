package com.typeqast.demo.integrationTest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.typeqast.demo.database.model.MeterData;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomConfiguration;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.service.meterData.MeterDataService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import({CustomConfiguration.class})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MeterDataControllerIT {

    @Resource
    private MeterDataService meterDataService;

    @LocalServerPort
    int randomServerPort;

    @Resource
    private RestTemplateBuilder restTemplateBuilder;

    @Resource
    ResourceLoader resourceLoader;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created meterData ID
    private static long CREATED_METER_DATA_ID = 3L;


    @Test
    public void getSingleMeterData() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource meterDataExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meterData/ResponseMeterDataExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(meterDataExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/meterdata/1", String.class);

        // Then
        // Remove random port
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void getSingleMeterDataThatDoesNotExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource meterDataExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meterData/ResponseMeterDataDoesntExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(meterDataExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response;
        try {
            response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                    + "/api/meterdata/5", String.class);
        } catch (HttpClientErrorException httpMeterDataErrorException) {
            response = httpMeterDataErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        JSONObject removeTimestamp = responseToMatch.getJSONObject("_embedded").getJSONObject("error");
        removeTimestamp.remove("timestamp");
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    void getMultipleMeterData() throws Exception {
        // Given
        org.springframework.core.io.Resource meterDataExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meterData/ResponseMultipleMeterDataExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(meterDataExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        JSONObject expectedResponse = new JSONObject(stringBuilder.toString());

        // When
        String response = restTemplateBuilder.build().getForObject("http://localhost:" + randomServerPort
                + "/api/meterdata", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response.replace(":" + randomServerPort, ""));
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    @Order(1)
    void createMeterData() throws Exception {
        // When
        String response = restTemplateBuilder.build().postForObject("http://localhost:" + randomServerPort
                + "/api/meterdata", CustomSuplier.createMeterDataWithId(5L), String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response).getJSONObject("_embedded").getJSONObject("meterData");
        responseToMatch.remove("_links");
        CREATED_METER_DATA_ID = responseToMatch.getLong("meterDataId");

        String meterDataFromDatabase = objectMapper.writer().writeValueAsString(meterDataService.findById(CREATED_METER_DATA_ID));
        JSONObject expectedResponse = new JSONObject(meterDataFromDatabase);
        expectedResponse.getJSONObject("client").getJSONObject("meter").remove("links");
        expectedResponse.getJSONObject("client").remove("links");
        expectedResponse.remove("links");
        assertEquals(expectedResponse.toString(), responseToMatch.toString());
    }

    @Test
    @Order(2)
    void updateMeterData() throws Exception {
        // Given
        MeterData meterData = MeterData.builder().quantity(10L).build();

        // When
        restTemplateBuilder.build().put("http://localhost:" + randomServerPort
                + "/api/meterdata/" + CREATED_METER_DATA_ID, meterData);

        // Then
        MeterData updatedMeterData = meterDataService.findById(CREATED_METER_DATA_ID);
        assertEquals(10L, updatedMeterData.getQuantity());
    }

    @Test
    @Order(3)
    void deleteMeterData() throws Exception {
        // When
        restTemplateBuilder.build().delete("http://localhost:" + randomServerPort
                + "/api/meterdata/" + CREATED_METER_DATA_ID);

        // Then
        assertThrows(ResourceNotFoundException.class, () -> meterDataService.findById(CREATED_METER_DATA_ID));
    }
}
