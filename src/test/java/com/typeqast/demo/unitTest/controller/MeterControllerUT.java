package com.typeqast.demo.unitTest.controller;

import com.typeqast.demo.controller.MeterController;
import com.typeqast.demo.unitTest.service.MeterServiceMockedModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MeterController.class)
@Import({MeterServiceMockedModule.class})
public class MeterControllerUT {

    @Resource
    MockMvc mockMvc;

    @Resource
    private MeterServiceMockedModule meterServiceMockedModule;

    @Resource
    ResourceLoader resourceLoader;

    @AfterEach
    void mockCleanup() {
        meterServiceMockedModule.resetMock();
    }

    @Test
    void getMultipleMeters() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meter/ResponseMultipleMetersExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        meterServiceMockedModule.givenThatMultipleMeterExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/meter")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

}
