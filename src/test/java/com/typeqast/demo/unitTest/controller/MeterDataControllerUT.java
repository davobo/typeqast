package com.typeqast.demo.unitTest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.typeqast.demo.controller.MeterDataController;
import com.typeqast.demo.database.model.MeterData;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.unitTest.service.MeterDataServiceMockedModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MeterDataController.class)
@Import({MeterDataServiceMockedModule.class})
public class MeterDataControllerUT {

    @Resource
    MockMvc mockMvc;

    @Resource
    private MeterDataServiceMockedModule meterDataServiceMockedModule;

    @Resource
    ResourceLoader resourceLoader;

    @AfterEach
    void mockCleanup() {
        meterDataServiceMockedModule.resetMock();
    }

    @Test
    void getSingleMeterData() throws Exception {
        // Given
        org.springframework.core.io.Resource meterDataExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meterData/ResponseMeterDataExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(meterDataExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        meterDataServiceMockedModule.givenThatMeterDataExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/meterdata/1")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void getSingleMeterDataThatDoesNotExists() throws Exception {
        // Given
        org.springframework.core.io.Resource meterDataDoesntExistJsonResponse =
                resourceLoader.getResource("classpath:controller/meterData/ResponseMeterDataDoesntExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(meterDataDoesntExistJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        meterDataServiceMockedModule.givenThatMeterDataDoesNotExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/meterdata/5")).andExpect(status().is4xxClientError());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void getMultipleMeterData() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meterData/ResponseMultipleMeterDataExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        meterDataServiceMockedModule.givenThatMultipleMeterDataExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/meterdata")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void createMeterData() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/meterData/ResponseMeterDataExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        String createClient = new ObjectMapper().writeValueAsString(CustomSuplier.createClientWithId(3L));
        meterDataServiceMockedModule.givenThatMeterDataIsCreated();

        // When
        ResultActions resultActions = mockMvc.perform(post("/api/meterdata").contentType(MediaType.APPLICATION_JSON)
                .content(createClient)).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void updateMeterData() throws Exception {
        // Given
        meterDataServiceMockedModule.givenThatMeterDataExistsOnUpdate();
        String updateMeterData = new ObjectMapper().writeValueAsString(MeterData.builder()
                .quantity(110L).build());

        // When
        ResultActions resultActions = mockMvc.perform(put("/api/meterdata/1").contentType(MediaType.APPLICATION_JSON)
                .content(updateMeterData)).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().string(""));
    }

    @Test
    void deleteClient() throws Exception {
        // Given
        meterDataServiceMockedModule.givenThatDataMeterExistsOnDelete();

        // When
        ResultActions resultActions = mockMvc.perform(delete("/api/meterdata/1")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().string(""));
    }
}
