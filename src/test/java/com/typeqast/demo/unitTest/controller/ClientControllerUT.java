package com.typeqast.demo.unitTest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.typeqast.demo.controller.ClientController;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.unitTest.service.ClientServiceMockedModule;
import com.typeqast.demo.unitTest.service.MeterDataServiceMockedModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ClientController.class)
@Import({ClientServiceMockedModule.class,
        MeterDataServiceMockedModule.class})
public class ClientControllerUT {

    @Resource
    MockMvc mockMvc;

    @Resource
    private ClientServiceMockedModule clientServiceMockedModule;

    @Resource
    private MeterDataServiceMockedModule meterDataServiceMockedModule;

    @Resource
    ResourceLoader resourceLoader;

    @AfterEach
    void mockCleanup() {
        clientServiceMockedModule.resetMock();
        meterDataServiceMockedModule.resetMock();
    }

    @Test
    void getSingleClient() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseClientExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        clientServiceMockedModule.givenThatClientExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/client/1")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void getSingleClientThatDoesNotExists() throws Exception {
        // Given
        org.springframework.core.io.Resource clientDoesntExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseClientDoesntExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientDoesntExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        clientServiceMockedModule.givenThatClientDoesNotExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/client/3")).andExpect(status().is4xxClientError());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void getDataByYear() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMeterDataByYearExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        meterDataServiceMockedModule.givenThatDataByYearExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/client/1/year")).andExpect(status().is2xxSuccessful());

        ResultMatcher resultMatcher = content().json(stringBuilder.toString());
        // Then
        resultActions.andExpect(resultMatcher);
    }

    @Test
    void getDataBySpecificYear() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMeterDataBySpecificYearExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        meterDataServiceMockedModule.givenThatDataBySpecificYearExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/client/1/year/2018")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void getDataBySpecificMonth() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMeterDataByMonthExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        meterDataServiceMockedModule.givenThatDataBySpecificMonthExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/client/1/year/2018/2")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void getMultipleClients() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseMultipleClientsExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        clientServiceMockedModule.givenThatMultipleClientsExists();

        // When
        ResultActions resultActions = mockMvc.perform(get("/api/client")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void createClient() throws Exception {
        // Given
        org.springframework.core.io.Resource clientExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/client/ResponseClientExist.json");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientExistsJsonResponse.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        String createClient = new ObjectMapper().writeValueAsString(CustomSuplier.createClientWithId(3L));
        clientServiceMockedModule.givenThatClientIsCreated();

        // When
        ResultActions resultActions = mockMvc.perform(post("/api/client").contentType(MediaType.APPLICATION_JSON)
                .content(createClient)).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().json(stringBuilder.toString()));
    }

    @Test
    void updateClient() throws Exception {
        // Given
        clientServiceMockedModule.givenThatClientExistsOnUpdate();
        String updateClient = new ObjectMapper().writeValueAsString(Client.builder()
                .address("some new address").build());

        // When
        ResultActions resultActions = mockMvc.perform(put("/api/client/3").contentType(MediaType.APPLICATION_JSON)
                .content(updateClient)).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().string(""));
    }

    @Test
    void deleteClient() throws Exception {
        // Given
        clientServiceMockedModule.givenThatClientExistsOnDelete();

        // When
        ResultActions resultActions = mockMvc.perform(delete("/api/client/3")).andExpect(status().is2xxSuccessful());

        // Then
        resultActions.andExpect(content().string(""));
    }
}
