package com.typeqast.demo.unitTest.service;

import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.database.model.MeterData;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.service.meterData.MeterDataService;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@TestConfiguration
public class MeterDataServiceMockedModule {

    private final MeterDataService meterDataService = Mockito.mock(MeterDataService.class);

    @Bean
    @Primary
    MeterDataService meterDataServiceMock() {
        return meterDataService;
    }

    public void givenThatMeterDataExists() {
        given(meterDataService.findById(any())).willReturn(CustomSuplier.createMeterDataWithId(1L));
        System.out.println("break point");
    }

    public void givenThatMeterDataDoesNotExists() {
        given(meterDataService.findById(any())).willThrow(new ResourceNotFoundException("MeterData with id 5 not found"));
    }

    public void givenThatMultipleMeterDataExists() {
        List<MeterData> meterDataList = new ArrayList<>();
        meterDataList.add(CustomSuplier.createMeterDataWithId(1L, LocalDate.of(2018, 1, 5), 2L));
        meterDataList.add(CustomSuplier.createMeterDataWithId(2L, LocalDate.of(2018, 2, 5), 5L));
        meterDataList.add(CustomSuplier.createMeterDataWithId(3L, LocalDate.of(2018, 3, 5), 7L));
        meterDataList.add(CustomSuplier.createMeterDataWithId(4L, LocalDate.of(2018, 4, 5), 21L));
        PageRequest pageRequest = PageRequest.of(0, 20);
        given(meterDataService.findAll(any())).willReturn(new PageImpl<>(meterDataList, pageRequest, 1));
    }

    public void givenThatMeterDataIsCreated() {
        given(meterDataService.create(any())).willReturn(CustomSuplier.createMeterDataWithId(1L, LocalDate.of(2018, 1, 5), 2L));
    }

    public void givenThatMeterDataExistsOnUpdate() {
        given(meterDataService.update(any(), any())).willReturn(CustomSuplier.createMeterDataWithId(1L));
    }

    public void givenThatDataMeterExistsOnDelete() {
        doNothing().when(meterDataService).delete(any(Long.class));
    }

    public void givenThatDataByYearExists() {
        given(meterDataService.getMeterDataByYear(any(), any())).willReturn(CustomSuplier.createMeterDataByYear());
    }

    public void givenThatDataBySpecificYearExists() {
        given(meterDataService.getMeterDataBySpecificYear(any(), any())).willReturn(CustomSuplier.createMeterDataBySpecificYear());
    }

    public void givenThatDataBySpecificMonthExists() {
        given(meterDataService.getMeterDataBySpecificMonth(any(), any(), any())).willReturn(CustomSuplier.createMeterDataBySpecificMonth());
    }

    public void resetMock() {
        Mockito.reset(meterDataService);
    }

}
