package com.typeqast.demo.unitTest.service;

import com.typeqast.demo.database.model.Meter;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.service.meter.MeterEnum;
import com.typeqast.demo.service.meter.MeterService;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@TestConfiguration
public class MeterServiceMockedModule {

    private final MeterService meterService = Mockito.mock(MeterService.class);

    @Bean
    @Primary
    MeterService meterServiceMock() {
        return meterService;
    }

    public void givenThatMultipleMeterExists() {
        List<Meter> meterDataList = new ArrayList<>();
        meterDataList.add(CustomSuplier.createMeterWithId(1L, MeterEnum.ELECTRIC));
        meterDataList.add(CustomSuplier.createMeterWithId(2L, MeterEnum.WATER));
        PageRequest pageRequest = PageRequest.of(0, 20);
        given(meterService.findAll(any())).willReturn(new PageImpl<>(meterDataList, pageRequest, 1));
    }

    public void resetMock() {
        Mockito.reset(meterService);
    }

}
