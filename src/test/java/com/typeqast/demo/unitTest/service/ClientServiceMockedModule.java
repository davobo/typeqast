package com.typeqast.demo.unitTest.service;

import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomSuplier;
import com.typeqast.demo.service.client.ClientService;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@TestConfiguration
public class ClientServiceMockedModule {

    private final ClientService clientServiceMock = Mockito.mock(ClientService.class);

    @Bean
    @Primary
    ClientService clientServiceMock() {
        return clientServiceMock;
    }

    public void givenThatClientExists() {
        given(clientServiceMock.findById(any())).willReturn(CustomSuplier.createClientWithId(1L));
    }

    public void givenThatClientDoesNotExists() {
        given(clientServiceMock.findById(any())).willThrow(new ResourceNotFoundException("Client with id 3 not found"));
    }

    public void givenThatMultipleClientsExists() {
        List<Client> clientList = new ArrayList<>();
        clientList.add(CustomSuplier.createClientWithId(1L));
        clientList.add(CustomSuplier.createClientWithId(2L));
        PageRequest pageRequest = PageRequest.of(0, 20);
        given(clientServiceMock.findAll(any())).willReturn(new PageImpl<>(clientList, pageRequest, 1));
    }

    public void givenThatClientExistsOnUpdate() {
        given(clientServiceMock.update(any(), any())).willReturn(CustomSuplier.createClientWithId(1L));
    }

    public void givenThatClientDoesNotExistsOnUpdate() {
        given(clientServiceMock.update(any(), any())).willThrow(new ResourceNotFoundException("Client with id not found"));
    }

    public void givenThatClientIsCreated() {
        given(clientServiceMock.create(any())).willReturn(CustomSuplier.createClientWithId(1L));
    }

    public void givenThatClientExistsOnDelete() {
        doNothing().when(clientServiceMock).delete(any(Long.class));
    }

    public void resetMock() {
        Mockito.reset(clientServiceMock);
    }

}
