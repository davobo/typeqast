package com.typeqast.demo.service.client;

import com.typeqast.demo.database.dao.ClientDAO;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomBeanUtils;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    /**
     * Logger
     */
    private static final Logger logger = LogManager.getLogger(ClientServiceImpl.class);

    /**
     * Object used to perform CRUD operations on "client" table
     */
    private final ClientDAO clientDAO;

    @Override
    public Page<Client> findAll(Pageable pageable) {
        return clientDAO.findAll(pageable);
    }

    @Override
    public Client findById(Long id) {
        return clientDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Client with id " + id + " not found"));
    }

    @Override
    @Transactional
    public Client create(Client client) {
        return clientDAO.saveAndFlush(client);
    }

    @Override
    @Transactional
    public Client update(Long clientId, Client clientRequest) {
        Client clientToUpdate = findById(clientId);

        BeanUtils.copyProperties(clientRequest, clientToUpdate, CustomBeanUtils.getNullPropertyNames(clientRequest));
        return clientDAO.saveAndFlush(clientToUpdate);
    }

    @Override
    @Transactional
    public void delete(Long clientId) {
        clientDAO.deleteById(clientId);
    }
}
