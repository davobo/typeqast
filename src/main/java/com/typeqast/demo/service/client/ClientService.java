package com.typeqast.demo.service.client;

import com.typeqast.demo.database.model.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface ClientService {
    /**
     * Interface for fetching all clients from "client" table
     *
     * @param pageable Pageable
     * @return Page<Client>
     */
    Page<Client> findAll(Pageable pageable);

    /**
     * Interface for fetching individual client from "client" table based on its ID
     *
     * @param id Integer
     * @return Client
     */
    Client findById(Long id);

    /**
     * Interface to store new client in "client" table
     *
     * @param client Client
     * @return Client
     */
    Client create(Client client);

    /**
     * Interface to update existing client
     *
     * @param clientId      Integer
     * @param clientRequest Client
     * @return Client
     */
    Client update(Long clientId, Client clientRequest);

    /**
     * Interface to remove client from "client" table by provided id
     *
     * @param clientId Integer
     * @return ResponseEntity<?>
     */
    void delete(Long clientId);
}
