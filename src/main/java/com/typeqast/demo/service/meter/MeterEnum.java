package com.typeqast.demo.service.meter;

public enum MeterEnum {
    ELECTRIC,
    POWER,
    GAS,
    WATER;
}
