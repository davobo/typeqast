package com.typeqast.demo.service.meter;

import com.typeqast.demo.database.model.Meter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface MeterService {
    /**
     * Interface for fetching all meters from "meter" table
     *
     * @param pageable Pageable
     * @return Page<Meter>
     */
    Page<Meter> findAll(Pageable pageable);

    /**
     * Interface for fetching individual meter from "meter" table based on its ID
     *
     * @param id Integer
     * @return Meter
     */
    Meter findById(Long id);

    /**
     * Interface to store new meter in "meter" table
     *
     * @param meter Meter
     * @return Meter
     */
    Meter create(Meter meter);

    /**
     * Interface to update existing meter
     *
     * @param meterId      Integer
     * @param meterRequest Meter
     * @return Meter
     */
    Meter update(Long meterId, Meter meterRequest);

    /**
     * Interface to remove meter from "meter" table by provided id
     *
     * @param meterId Integer
     * @return ResponseEntity<?>
     */
    void delete(Long meterId);
}
