package com.typeqast.demo.service.meter;

import com.typeqast.demo.database.dao.MeterDAO;
import com.typeqast.demo.database.model.Meter;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomBeanUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MeterServiceImpl implements MeterService {

    /**
     * Object used to perform CRUD operations on "meter" table
     */
    private final MeterDAO meterDAO;

    @Override
    public Page<Meter> findAll(Pageable pageable) {
        return meterDAO.findAll(pageable);
    }

    @Override
    public Meter findById(Long id) {
        return meterDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Meter with id " + id + " not found"));
    }

    @Override
    public Meter create(Meter meter) {
        return meterDAO.saveAndFlush(meter);
    }

    @Override
    public Meter update(Long meterId, Meter meterRequest) {
        Meter meterToUpdate = findById(meterId);

        BeanUtils.copyProperties(meterRequest, meterToUpdate, CustomBeanUtils.getNullPropertyNames(meterRequest));
        return meterDAO.saveAndFlush(meterToUpdate);
    }

    @Override
    public void delete(Long meterId) {
        meterDAO.deleteById(meterId);
    }
}
