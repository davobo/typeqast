package com.typeqast.demo.service.meterData;

import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonthDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificYearDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataByYearsDTO;
import com.typeqast.demo.database.dao.MeterDataDAO;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.database.model.MeterData;
import com.typeqast.demo.exception.ResourceNotFoundException;
import com.typeqast.demo.helper.CustomBeanUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MeterDataServiceImpl implements MeterDataService {

    /**
     * Object used to perform CRUD operations on "meter_data" table
     */
    private final MeterDataDAO meterDataDAO;

    @Override
    public Page<MeterData> findAll(Pageable pageable) {
        return meterDataDAO.findAll(pageable);
    }

    @Override
    public MeterData findById(Long id) {
        return meterDataDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("MeterData with id " + id + " not found"));
    }

    @Override
    public Page<MeterDataByYearsDTO> getMeterDataByYear(Client client, Pageable pageable) {
        return meterDataDAO.getDataByYears(client, pageable);
    }

    @Override
    public MeterDataBySpecificYearDTO getMeterDataBySpecificYear(Client client, Integer year) {
        return new MeterDataBySpecificYearDTO(year, meterDataDAO.getDataBySpecificYear(client, year));
    }

    @Override
    public MeterDataBySpecificMonthDTO getMeterDataBySpecificMonth(Client client, Integer year, Integer month) {
        return new MeterDataBySpecificMonthDTO(year, meterDataDAO.getDataBySpecificMonth(client, year, month));
    }

    @Override
    public MeterData create(MeterData meterData) {
        return meterDataDAO.saveAndFlush(meterData);
    }

    @Override
    public MeterData update(Long meterDataId, MeterData meterDataRequest) {
        MeterData meterDataToUpdate = findById(meterDataId);

        BeanUtils.copyProperties(meterDataRequest, meterDataToUpdate, CustomBeanUtils.getNullPropertyNames(meterDataRequest));
        return meterDataDAO.saveAndFlush(meterDataToUpdate);
    }

    @Override
    public void delete(Long meterDataId) {
        meterDataDAO.deleteById(meterDataId);
    }
}
