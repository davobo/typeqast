package com.typeqast.demo.service.meterData;

import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonthDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificYearDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataByYearsDTO;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.database.model.MeterData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface MeterDataService {

    /**
     * Interface for fetching all meterData from "meter_data" table
     *
     * @param pageable Pageable
     * @return Page<MeterData>
     */
    Page<MeterData> findAll(Pageable pageable);

    /**
     * Interface for fetching individual meterData from "meter_data" table based on its ID
     *
     * @param id Integer
     * @return MeterData
     */
    MeterData findById(Long id);

    /**
     * Interface for fetching meter data, grouped by each year, from "meterData" table based on client ID
     *
     * @param client Integer
     * @return Page<MeterDataByYearsDTO>
     */
    Page<MeterDataByYearsDTO> getMeterDataByYear(Client client, Pageable pageable);

    /**
     * Interface for fetching meter data, grouped by each year, from "meterData" table based on client ID
     *
     * @param client Integer
     * @param year   int
     * @return List<MeterDataByYearsDTO>
     */
    MeterDataBySpecificYearDTO getMeterDataBySpecificYear(Client client, Integer year);

    /**
     * Interface for fetching meter data for single month from "meterData" table based on client ID, year and month.
     *
     * @param client Integer
     * @param year   int
     * @param month  Integer
     * @return List<MeterDataByYearsDTO>
     */
    MeterDataBySpecificMonthDTO getMeterDataBySpecificMonth(Client client, Integer year, Integer month);

    /**
     * Interface to store new meterData in "meter_data" table
     *
     * @param meterData MeterData
     * @return MeterData
     */
    MeterData create(MeterData meterData);

    /**
     * Interface to update existing meterData
     *
     * @param meterDataId      Integer
     * @param meterDataRequest MeterData
     * @return MeterData
     */
    MeterData update(Long meterDataId, MeterData meterDataRequest);

    /**
     * Interface to remove meterData from "meter_data" table by provided id
     *
     * @param meterDataId Integer
     * @return ResponseEntity<?>
     */
    void delete(Long meterDataId);
}
