package com.typeqast.demo.controller;

import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonthDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificYearDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataByYearsDTO;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.service.client.ClientService;
import com.typeqast.demo.service.meterData.MeterDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/client")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;
    private final MeterDataService meterDataService;

    @GetMapping
    public PagedModel<EntityModel<Client>> getAllClients(Pageable pageable, PagedResourcesAssembler<Client> assembler) {
        Page<Client> pageArticles = clientService.findAll(pageable);
        pageArticles.forEach(client -> client.add(linkTo(methodOn(ClientController.class)
                .getClientById(client.getClientId())).withSelfRel()));
        return assembler.toModel(pageArticles);
    }

    /**
     * Rest interface to fetch specific client from DB by id.
     *
     * @param clientId Integer
     * @return EntityModel<Client>
     */
    @GetMapping(path = "/{clientId}")
    public RepresentationModel<Client> getClientById(@PathVariable Long clientId) {
        Client client = clientService.findById(clientId);
        return HalModelBuilder.emptyHalModel().embed(client).link(linkTo(methodOn(ClientController.class)
                .getClientById(clientId)).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch total meter data of specific client, sorted by years.
     *
     * @param clientId Integer
     * @return EntityModel<Client>
     */
    @GetMapping(path = "/{clientId}/year")
    public PagedModel<EntityModel<MeterDataByYearsDTO>> getDataByYear(@PathVariable Long clientId, Pageable pageable, PagedResourcesAssembler<MeterDataByYearsDTO> assembler) {
        Page<MeterDataByYearsDTO> meterData = meterDataService.getMeterDataByYear(clientService.findById(clientId), pageable);
        return assembler.toModel(meterData);
    }

    /**
     * Rest interface to fetch total meter data of specific client in specific year.
     *
     * @param clientId Integer
     * @param year     Integer
     * @return EntityModel<Client>
     */
    @GetMapping(path = "/{clientId}/year/{year}")
    public RepresentationModel<MeterDataBySpecificYearDTO> getDataBySpecificYear(@PathVariable Long clientId, @PathVariable Integer year) {
        MeterDataBySpecificYearDTO meterDataBySpecificYearDTO = meterDataService.getMeterDataBySpecificYear(clientService.findById(clientId), year);
        return HalModelBuilder.emptyHalModel().embed(meterDataBySpecificYearDTO).link(linkTo(methodOn(ClientController.class)
                .getDataBySpecificYear(clientId, year)).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch total meter data of specific client, year and month.
     *
     * @param clientId Integer
     * @param year     Integer
     * @param month    Integer
     * @return EntityModel<Client>
     */
    @GetMapping(path = "/{clientId}/year/{year}/{month}")
    public RepresentationModel<MeterDataBySpecificMonthDTO> getDataBySpecificMonth(@PathVariable Long clientId, @PathVariable Integer year, @PathVariable Integer month) {
        MeterDataBySpecificMonthDTO meterDataBySpecificMonthDTO = meterDataService.getMeterDataBySpecificMonth(clientService.findById(clientId), year, month);
        return HalModelBuilder.emptyHalModel().embed(meterDataBySpecificMonthDTO).link(linkTo(methodOn(ClientController.class)
                .getDataBySpecificMonth(clientId, year, month)).withSelfRel()).build();
    }

    /**
     * Rest interface to create new Client.
     *
     * @param clientDTO Client
     * @return Client
     */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public RepresentationModel<Client> create(@RequestBody Client clientDTO) {
        Client createdClient = clientService.create(clientDTO);
        return HalModelBuilder.emptyHalModel().embed(createdClient).link(linkTo(methodOn(ClientController.class)
                .getClientById(createdClient.getClientId())).withSelfRel()).build();
    }

    /**
     * Rest interface to update client under specified ID.
     *
     * @param clientId  Long
     * @param clientDTO Client
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("{clientId}")
    public void update(@PathVariable Long clientId, @Valid @RequestBody Client clientDTO) {
        clientService.update(clientId, clientDTO);
    }

    /**
     * Rest interface to delete article.
     *
     * @param clientId Long
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{clientId}")
    public void delete(@PathVariable Long clientId) {
        clientService.delete(clientId);
    }
}
