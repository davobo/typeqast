package com.typeqast.demo.controller;

import com.typeqast.demo.database.model.Meter;
import com.typeqast.demo.service.meter.MeterService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/meter")
@RequiredArgsConstructor
public class MeterController {

    private final MeterService meterService;

    @GetMapping
    public PagedModel<EntityModel<Meter>> getAllMeters(Pageable pageable, PagedResourcesAssembler<Meter> assembler) {
        Page<Meter> pageArticles = meterService.findAll(pageable);
        pageArticles.forEach(meter -> {
            meter.add(linkTo(methodOn(MeterController.class)
                    .getAllMeters(pageable, assembler)).withSelfRel());
        });
        return assembler.toModel(pageArticles);
    }
}
