package com.typeqast.demo.controller;

import com.typeqast.demo.database.model.MeterData;
import com.typeqast.demo.service.meterData.MeterDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/meterdata")
@RequiredArgsConstructor
public class MeterDataController {

    private final MeterDataService meterDataService;

    @GetMapping
    public PagedModel<EntityModel<MeterData>> getAllMeterData(Pageable pageable, PagedResourcesAssembler<MeterData> assembler) {
        Page<MeterData> pageArticles = meterDataService.findAll(pageable);
        pageArticles.forEach(meterData -> {
            meterData.add(linkTo(methodOn(MeterDataController.class)
                    .getMeterDataById(meterData.getMeterDataId())).withSelfRel());
        });
        return assembler.toModel(pageArticles);
    }

    /**
     * Rest interface to fetch specific meterData from DB by id
     *
     * @param meterDataId Integer
     * @return EntityModel<MeterData>
     */
    @GetMapping(path = "/{meterDataId}")
    public RepresentationModel<MeterData> getMeterDataById(@PathVariable Long meterDataId) {
        MeterData meterData = meterDataService.findById(meterDataId);
        return HalModelBuilder.emptyHalModel().embed(meterData).link(linkTo(methodOn(MeterDataController.class)
                .getMeterDataById(meterDataId)).withSelfRel()).build();
    }

    /**
     * Rest interface to create new MeterData
     *
     * @param meterDataDTO MeterData
     * @return MeterData
     */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public RepresentationModel<MeterData> create(@RequestBody MeterData meterDataDTO) {
        MeterData updatedMeterData = meterDataService.create(meterDataDTO);
        return HalModelBuilder.emptyHalModel().embed(updatedMeterData).link(linkTo(methodOn(MeterDataController.class)
                .getMeterDataById(updatedMeterData.getMeterDataId())).withSelfRel()).build();
    }

    /**
     * Rest interface to update meterData under specified ID
     *
     * @param meterDataId  Long
     * @param meterDataDTO MeterData
     * @return MeterData
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("{meterDataId}")
    public void update(@PathVariable Long meterDataId, @Valid @RequestBody MeterData meterDataDTO) {
        meterDataService.update(meterDataId, meterDataDTO);
    }

    /**
     * Rest interface to delete article
     *
     * @param meterDataId Long
     * @return ResponseEntity<?>
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{meterDataId}")
    public void delete(@PathVariable Long meterDataId) {
        meterDataService.delete(meterDataId);
    }
}
