package com.typeqast.demo.controller.dto.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificYearDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonth;

import java.io.IOException;

public class MeterDataBySpecificYearSerializer extends JsonSerializer<MeterDataBySpecificYearDTO> {

    private final String YEAR_STRING = "year";

    @Override
    public void serialize(MeterDataBySpecificYearDTO value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField(YEAR_STRING, value.getYear());
        for (MeterDataBySpecificMonth meterDataBySpecificMonth : value.getMeterDataBySpecificMonthList()) {
            String month = meterDataBySpecificMonth.getMonth().toString();
            gen.writeNumberField(Character.toUpperCase(month.charAt(0)) + month.substring(1).toLowerCase(), meterDataBySpecificMonth.getValue());
        }
        gen.writeEndObject();
    }
}
