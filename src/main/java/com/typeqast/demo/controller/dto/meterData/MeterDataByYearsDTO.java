package com.typeqast.demo.controller.dto.meterData;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.server.core.Relation;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@Relation(collectionRelation = "yearData")
public class MeterDataByYearsDTO implements Serializable {

    int year;
    long total;

    // Since MeterData table is defined as date, JPQL requires Date, long constructor
    public MeterDataByYearsDTO(LocalDate year, long total) {
    }
}
