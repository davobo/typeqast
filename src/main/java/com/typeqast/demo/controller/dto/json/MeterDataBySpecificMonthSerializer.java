package com.typeqast.demo.controller.dto.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonthDTO;

import java.io.IOException;

public class MeterDataBySpecificMonthSerializer extends JsonSerializer<MeterDataBySpecificMonthDTO> {

    private final String YEAR_STRING = "year";

    @Override
    public void serialize(MeterDataBySpecificMonthDTO value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField(YEAR_STRING, value.getYear());
        String month = value.getMeterDataBySpecificMonth().getMonth().toString();
        gen.writeNumberField(Character.toUpperCase(month.charAt(0)) + month.substring(1).toLowerCase(), value.getMeterDataBySpecificMonth().getValue());
        gen.writeEndObject();
    }
}
