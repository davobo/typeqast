package com.typeqast.demo.controller.dto.meterData;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.typeqast.demo.controller.dto.json.MeterDataBySpecificMonthSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Builder
@AllArgsConstructor
@Getter
@JsonSerialize(using = MeterDataBySpecificMonthSerializer.class)
@Relation("monthData")
public class MeterDataBySpecificMonthDTO extends RepresentationModel<MeterDataBySpecificMonthDTO> {
    Integer year;
    MeterDataBySpecificMonth meterDataBySpecificMonth;
}
