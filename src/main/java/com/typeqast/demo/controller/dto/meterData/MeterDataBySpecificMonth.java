package com.typeqast.demo.controller.dto.meterData;

import lombok.Getter;

import java.time.Month;

@Getter
public class MeterDataBySpecificMonth {
    Month month;
    Long value;

    public MeterDataBySpecificMonth(Integer month, Long value) {
        this.month = Month.of(month);
        this.value = value;
    }
}
