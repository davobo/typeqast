package com.typeqast.demo.controller.dto.meterData;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.typeqast.demo.controller.dto.json.MeterDataBySpecificYearSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.util.List;

@Builder
@AllArgsConstructor
@Getter
@JsonSerialize(using = MeterDataBySpecificYearSerializer.class)
@Relation("singleYearData")
public class MeterDataBySpecificYearDTO extends RepresentationModel<MeterDataBySpecificYearDTO> {
    Integer year;
    List<MeterDataBySpecificMonth> meterDataBySpecificMonthList;
}
