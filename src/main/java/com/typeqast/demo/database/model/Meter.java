package com.typeqast.demo.database.model;

import com.typeqast.demo.service.meter.MeterEnum;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "meter")
public class Meter extends RepresentationModel<Meter> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "meter_id")
    @Setter(AccessLevel.NONE)
    private Long meterId;

    @Enumerated(EnumType.STRING)
    @Column(name = "meter_name")
    @NotEmpty
    private MeterEnum name;
}
