package com.typeqast.demo.database.model;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "meter_data")
public class MeterData extends RepresentationModel<MeterData> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "meter_data_id")
    @Setter(AccessLevel.NONE)
    private Long meterDataId;

    @Column(name = "meter_data_date")
    @NotEmpty
    private LocalDate date;

    @Column(name = "meter_data_quantity")
    @NotEmpty
    private Long quantity;

    @OneToOne
    @JoinColumn(name = "client", referencedColumnName = "client_id")
    @NotEmpty
    private Client client;
}
