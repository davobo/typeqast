package com.typeqast.demo.database.model;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * Client entity
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Relation("client")
@Table(name = "client")
public class Client extends RepresentationModel<Client> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    @Setter(AccessLevel.NONE)
    private Long clientId;

    @Column(name = "client_first_name")
    @NotEmpty
    private String firstName;

    @Column(name = "client_last_name")
    @NotEmpty
    private String lastName;

    @Column(name = "client_address")
    @NotEmpty
    private String address;

    @Column(name = "client_email")
    @NotEmpty
    @Email
    private String email;

    @OneToOne
    @JoinColumn(name = "meter", referencedColumnName = "meter_id")
    private Meter meter;
}
