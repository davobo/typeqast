package com.typeqast.demo.database.dao;

import com.typeqast.demo.controller.dto.meterData.MeterDataByYearsDTO;
import com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonth;
import com.typeqast.demo.database.model.Client;
import com.typeqast.demo.database.model.MeterData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MeterData repository interface
 */
@Repository
public interface MeterDataDAO extends JpaRepository<MeterData, Long> {

    @Query("SELECT new com.typeqast.demo.controller.dto.meterData.MeterDataByYearsDTO(YEAR(md.date)," +
            " SUM(md.quantity)) FROM MeterData as md WHERE md.client = :client GROUP BY YEAR(md.date)")
    Page<MeterDataByYearsDTO> getDataByYears(Client client, Pageable pageable);

    @Query("SELECT new com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonth(MONTH(md.date)," +
            " md.quantity) FROM MeterData as md WHERE md.client=:client and YEAR(md.date) = :year")
    List<MeterDataBySpecificMonth> getDataBySpecificYear(Client client, Integer year);

    @Query("SELECT new com.typeqast.demo.controller.dto.meterData.MeterDataBySpecificMonth(MONTH(md.date)," +
            " md.quantity) FROM MeterData as md WHERE md.client=:client and YEAR(md.date) = :year and MONTH(md.date) = :month")
    MeterDataBySpecificMonth getDataBySpecificMonth(Client client, Integer year, Integer month);
}
