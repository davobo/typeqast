package com.typeqast.demo.database.dao;

import com.typeqast.demo.database.model.Meter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Meter repository interface
 */
@Repository
public interface MeterDAO extends JpaRepository<Meter, Long> {
}
