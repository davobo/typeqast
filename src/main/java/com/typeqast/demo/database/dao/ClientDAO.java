package com.typeqast.demo.database.dao;

import com.typeqast.demo.database.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Client repository interface
 */
@Repository
public interface ClientDAO extends JpaRepository<Client, Long> {

}
