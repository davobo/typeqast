package com.typeqast.demo.exception.helper;

public class SqlResponseProcessor {
    public enum SqlResponses {

        CLIENT_ADDRESS_CONSTRAINT("PUBLIC.CLIENT(CLIENT_ADDRESS)");

        private final String message;

        private SqlResponses(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }

        public static SqlResponses ifContains(String stringToCheck) {
            for (SqlResponses enumValue : values()) {
                if (stringToCheck.contains(enumValue.getMessage())) {
                    return enumValue;
                }
            }
            return null;
        }
    }

    public static String generateResponse(String errorMessage) {
        return checkValidResponses(errorMessage);
    }

    private static String checkValidResponses(String errorMessage) {
        SqlResponses sqlResponses = SqlResponses.ifContains(errorMessage);
        switch (sqlResponses) {
            case CLIENT_ADDRESS_CONSTRAINT:
                return "Client with provided address already exists.";
            default:
                // TODO: Implement custom exception
                return "";
        }
    }
}
