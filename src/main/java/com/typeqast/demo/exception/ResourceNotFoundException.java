package com.typeqast.demo.exception;

/**
 * Class used to present resource not found exceptions
 */
public class ResourceNotFoundException extends RuntimeException {

    /**
     * Not found error constructor
     *
     * @param msg String
     */
    public ResourceNotFoundException(String msg) {
        super(msg);
    }
}
