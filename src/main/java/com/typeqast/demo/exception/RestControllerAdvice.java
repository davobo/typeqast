package com.typeqast.demo.exception;

import com.typeqast.demo.exception.helper.SqlResponseProcessor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * Central point for handling API errors
 * TODO:rfc7807 format
 */
@ControllerAdvice(basePackages = {"com.typeqast.demo.controller"})
public class RestControllerAdvice extends ResponseEntityExceptionHandler {

    /**
     * Method used for handling "not found" errors
     *
     * @param ex      ResourceNotFoundException
     * @param request HttpServletRequest
     * @return ResponseEntity<RepresentationModel < ErrorResponse>>
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    ResponseEntity<RepresentationModel<ErrorResponse>> handleResourceNotFoundException
    (ResourceNotFoundException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(HalModelBuilder.emptyHalModel().embed(error).link(Link.of(getFullURL(request))).build(), HttpStatus.NOT_FOUND);
    }

    /**
     * Method used for handling JPA errors on table constraints
     *
     * @param ex      DataIntegrityViolationException
     * @param request HttpServletRequest
     * @return ResponseEntity<RepresentationModel < ErrorResponse>>
     */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(DataIntegrityViolationException.class)
    ResponseEntity<RepresentationModel<ErrorResponse>> handleDataViolationException
    (DataIntegrityViolationException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.FORBIDDEN.getReasonPhrase(),
                HttpStatus.FORBIDDEN.value(), SqlResponseProcessor.generateResponse(ex.getMessage()));
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(HalModelBuilder.emptyHalModel().embed(error).link(Link.of(getFullURL(request))).build(), HttpStatus.FORBIDDEN);
    }

    /**
     * Build entire URL for HATEOAS link information
     *
     * @param request HttpServletRequest
     * @return String
     */
    private String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }
}
