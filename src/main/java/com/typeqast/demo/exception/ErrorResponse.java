package com.typeqast.demo.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDateTime;

/**
 * Error response that is returned in JSON
 */

@Data
@Relation(itemRelation = "error")
public class ErrorResponse extends RepresentationModel<ErrorResponse> {

    /**
     * Object representing error common name
     */
    private String reason;

    /**
     * Object representing error message
     */
    private String message;

    /**
     * Object representing error code
     */
    int code;

    /**
     * Object representing error timestamp
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    LocalDateTime timestamp;

    /**
     * Error response constructor
     *
     * @param reason   String
     * @param code    Integer
     * @param message String
     */
    public ErrorResponse(String reason, Integer code, String message) {
        this.code = code;
        this.reason = reason;
        this.message = message;
    }
}
